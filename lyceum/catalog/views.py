from django.http import HttpResponse


def item_list(request):
    return HttpResponse("Список элементов")


def item_detail(request, pk):
    return HttpResponse("Подробно элемент")


def positive_number(request, num):
    return HttpResponse(f"{num}")


def converter_positive_number(request, pk):
    return HttpResponse(f"{pk}")
