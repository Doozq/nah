from django.urls import path, re_path, register_converter

from . import converters, views

register_converter(converters.PositiveNumber, "positive")

urlpatterns = [
    path("", views.item_list),
    path("<int:pk>/", views.item_detail),
    re_path(r"re/(?P<num>[1-9][0-9]*)/", views.positive_number),
    path("converter/<positive:pk>/", views.converter_positive_number),
]
