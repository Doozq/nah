import catalog.models

import django.core.exceptions
from django.test import Client, TestCase


class StaticURLTests(TestCase):
    def test_catalog_endpoint(self):
        response = Client().get("/catalog/")
        self.assertEqual(response.status_code, 200)

    def test_catalog_int_endpoint(self):
        response = Client().get("/catalog/1/")
        self.assertEqual(response.status_code, 200)

    def test_catalog_re_int_endpoint(self):
        response = Client().get("/catalog/re/123/")
        self.assertEqual(response.status_code, 200)
        response = Client().get("/catalog/re/-123/")
        self.assertEqual(response.status_code, 404)
        response = Client().get("/catalog/re/0/")
        self.assertEqual(response.status_code, 404)
        response = Client().get("/catalog/re/2.3/")
        self.assertEqual(response.status_code, 404)
        response = Client().get("/catalog/re/string/")
        self.assertEqual(response.status_code, 404)

    def test_catalog_converter_int_endpoint(self):
        response = Client().get("/catalog/converter/123/")
        self.assertEqual(response.status_code, 200)
        response = Client().get("/catalog/converter/-123/")
        self.assertEqual(response.status_code, 404)
        response = Client().get("/catalog/converter/0/")
        self.assertEqual(response.status_code, 404)
        response = Client().get("/catalog/converter/2.3/")
        self.assertEqual(response.status_code, 404)
        response = Client().get("/catalog/converter/string/")
        self.assertEqual(response.status_code, 404)


class ModelsTests(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.category = catalog.models.Category.objects.create(
            is_published=True,
            name="Тестовая категория",
            slug="test-category-slug",
            weight=100,
        )
        cls.tag = catalog.models.Tag.objects.create(
            is_published=True, name="Тестовый тэг", slug="test-tag-slug"
        )

    def test_unable_item_create(self):
        item_count = catalog.models.Item.objects.count()
        with self.assertRaises(django.core.exceptions.ValidationError):
            self.item = catalog.models.Item(
                name="Тестовый товар",
                category=self.category,
                text="Нет нужного слова",
            )
            self.item.full_clean()
            self.item.save()
            self.item.tags.add(ModelsTests.tag)

        self.assertEqual(catalog.models.Item.objects.count(), item_count)

    def test_item_create(self):
        item_count = catalog.models.Item.objects.count()
        self.item = catalog.models.Item(
            name="Тестовый товар",
            category=self.category,
            text="Есть нужное слово: Превосходно",
        )
        self.item.full_clean()
        self.item.save()
        self.item.tags.add(ModelsTests.tag)

        self.assertEqual(catalog.models.Item.objects.count(), item_count + 1)
