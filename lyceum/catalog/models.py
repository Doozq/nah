from core.models import AbstractModel

import django.core.exceptions
import django.core.validators
import django.db.models


def excellent_validator(value):
    if all([x not in value.lower() for x in ["превосходно", "роскошно"]]):
        raise django.core.exceptions.ValidationError(
            "В тексте должно быть слово превосходно или роскошно"
        )


class Tag(AbstractModel):
    slug = django.db.models.CharField(
        "Слаг",
        max_length=200,
        unique=True,
        validators=[django.core.validators.validate_slug],
    )

    class Meta:
        verbose_name = "Тег"
        verbose_name_plural = "Теги"


class Category(AbstractModel):
    slug = django.db.models.CharField(
        "Слаг",
        max_length=200,
        unique=True,
        validators=[django.core.validators.validate_slug],
    )
    weight = django.db.models.IntegerField(
        "Вес",
        default=100,
        validators=[
            django.core.validators.MinValueValidator(0),
            django.core.validators.MaxValueValidator(32767),
        ],
    )

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    def __str__(self):
        return self.name


class Item(AbstractModel):
    category = django.db.models.ForeignKey(
        "category",
        default=None,
        verbose_name="Категория",
        related_name="category_items",
        on_delete=django.db.models.CASCADE,
    )
    tags = django.db.models.ManyToManyField(Tag, verbose_name="Теги")
    text = django.db.models.TextField(
        "Текст", validators=[excellent_validator]
    )

    class Meta:
        verbose_name = "Товар"
        verbose_name_plural = "Товары"
