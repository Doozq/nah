import django.db.models


class AbstractModel(django.db.models.Model):
    is_published = django.db.models.BooleanField("Опубликовано", default=True)
    name = django.db.models.CharField("Название", max_length=150)

    class Meta:
        abstract = True
