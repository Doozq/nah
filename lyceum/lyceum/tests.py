from django.test import Client, override_settings, TestCase


class MiddlewareTests(TestCase):
    def test_middleware_work(self):
        c = Client()
        for i in range(1, 22):
            response = c.get(f"/catalog/{i}/")
            if i % 10 != 0:
                self.assertEqual(
                    response.content.decode(), "Подробно элемент", i
                )
            else:
                self.assertEqual(
                    response.content.decode(), "онбордоП тнемелэ", i
                )

    @override_settings(ALLOW_REVERSE=False)
    def test_middleware_can_off(self):
        c = Client()
        for i in range(1, 22):
            response = c.get(f"/catalog/{i}/")
            self.assertEqual(response.content.decode(), "Подробно элемент", i)
