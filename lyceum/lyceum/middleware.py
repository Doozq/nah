import django.conf


class ReformatRequestMiddleware:
    response_count = 0

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        self.response_count += 1
        response = self.get_response(request)
        if self.response_count == 10 and django.conf.settings.ALLOW_REVERSE:
            words_list = response.content.decode().split()
            words_list = [i[::-1] for i in words_list]
            response.content = " ".join(words_list)
            self.response_count = 0
        return response
